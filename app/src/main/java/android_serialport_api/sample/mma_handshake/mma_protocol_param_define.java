
package android_serialport_api.sample.mma_handshake;

import android.os.Parcel;
import android.os.Parcelable;

public class mma_protocol_param_define implements Parcelable {
	// function error return value
	public static final int FUNCTION_RETURN_SUCCESS = 0;
	public static final int FUNCTION_RETURN_INTERRUPT_EXCEPT = -1;
	public static final int FUNCTION_RETURN_ARUGMENT_FAIL = -2;
	public static final int FUNCTION_RETURN_CASE_FAIL = -3;
	public static final int FUNCTION_RETURN_NO_RECEIVE_BUFFER = -4;
	public static final int FUNCTION_RETURN_SEND_PREPARE_FAIL = -5;
	public static final int FUNCTION_RETURN_SEND_PREPARE_DATA_SIZE_OVER = -6;
	public static final int FUNCTION_RETURN_NO_SEND_BUFFER = -7;
	public static final int FUNCTION_RETURN_RECEIVE_PACKET_COMPLETE = -8;
	public static final int FUNCTION_RETURN_WAIT_ACK_RESEND = -9;

    // mma ack status
	public static final byte MMA_ACK_STATUS_EXEC_SUCCESS = 0x00;
	public static final byte MMA_ACK_STATUS_FORMAT_HEADER_ERR = 0x01;
	public static final byte MMA_ACK_STATUS_FORMAT_LEN_UNSUPPORT = 0x02;
	public static final byte MMA_ACK_STATUS_FORMAT_LEN_UNMATCH = 0x03;
	public static final byte MMA_ACK_STATUS_FORMAT_CHECKSUM_ERR = 0x04;
	public static final byte MMA_ACK_STATUS_CMDID_UNSUPPORT = 0x06;
	public static final byte MMA_ACK_STATUS_CMDID_UNSUPPORT_WRITE = 0x07;
	public static final byte MMA_ACK_STATUS_CMDID_UNSUPPORT_READ = 0x08;
	public static final byte MMA_ACK_STATUS_EXEC_FAIL = 0x10;
	public static final byte MMA_ACK_STATUS_EXEC_LIMIT = 0x11;
	public static final byte MMA_ACK_STATUS_WAIT_ACK_TIMEOUT = 0x12;

	// Byte3,2 => command id, Byte1,0 => data length
	// System Video
	public static final int MMA_SV_CMD_ASPECT_RATIO                              = 0x08010001;
	public static final int MMA_SV_CMD_H_POSITION                                = 0x08020002;
	public static final int MMA_SV_CMD_V_POSITION                                = 0x08030002;
	public static final int MMA_SV_CMD_PHASE                                     = 0x08040002;
	public static final int MMA_SV_CMD_DIGITAL_ZOOM                              = 0x08050006;
	public static final int MMA_SV_CMD_H_SIZE                                    = 0x08060002;
	public static final int MMA_SV_CMD_V_KEYSTONE                                = 0x08070002;
	public static final int MMA_SV_CMD_H_KEYSTONE                                = 0x08080002;
	public static final int MMA_SV_CMD_FOUR_CORNER_ENABLE                        = 0x08090001;
	public static final int MMA_SV_CMD_FREEZE_IMAGE                              = 0x080A0001;
	public static final int MMA_SV_CMD_BLANKING_IMAGE                            = 0x080B0001;
	public static final int MMA_SV_CMD_COLOR_SPACE_CONVERSION                    = 0x080C0001;
	public static final int MMA_SV_CMD_3D_FORMAT                                 = 0x080D0001;
	public static final int MMA_SV_CMD_3D_SYNC_INVERT                            = 0x080E0001;
	public static final int MMA_SV_CMD_ROJECTOR_POSITION                         = 0x080F0001;
	public static final int MMA_SV_CMD_HDMI_COLOR_RANGE                          = 0x08100001;
	public static final int MMA_SV_CMD_FOUR_CORNER                               = 0x08110003;
	public static final int MMA_SV_CMD_3D_IMAGE                                  = 0x08120001;
	public static final int MMA_SV_CMD_3D_MODE                                   = 0x08130001;
	public static final int MMA_SV_CMD_SIGNAL_AUTOMATIC                          = 0x08140001;
	public static final int MMA_SV_CMD_TEST_PATTERN                              = 0x08150001;
	public static final int MMA_SV_CMD_SAVE_3D_SETTINGS                          = 0x08160008;
	public static final int MMA_SV_CMD_APPLY_3D_SETTINGS                         = 0x08170001;
	public static final int MMA_SV_CMD_FOUR_CORNER_ALL                           = 0x08180008;
	public static final int MMA_SV_CMD_HV_KEYSTONE                               = 0x08190005;
	public static final int MMA_SV_CMD_EDGE_MASK                                 = 0x081A0002;
	public static final int MMA_SV_CMD_IMAGE_SHIFT_H                             = 0x081B0002;
	public static final int MMA_SV_CMD_IMAGE_SHIFT_V                             = 0x081C0002;
	public static final int MMA_SV_CMD_SCREEN_TYPE                               = 0x081D0001;
	public static final int MMA_SV_CMD_HDMI_LINK_SETTING                         = 0x081E0004;
	public static final int MMA_SV_CMD_FOUR_CORNER_UINT                          = 0x081F0001;
	public static final int MMA_SV_CMD_VGA_AUTO_OPTIMIZATION                     = 0x08200001;
	public static final int MMA_SV_CMD_COMPONENT_DEFAULT_SETTING                 = 0x0821007E;
	public static final int MMA_SV_CMD_AUTO_APPLY_3D                             = 0x0822000C;
	public static final int MMA_SV_CMD_PBP_ONOFF                                 = 0x08230001;
	public static final int MMA_SV_CMD_PIP_ONOFF                                 = 0x08240001;
	public static final int MMA_SV_CMD_PIP_LOCATION                              = 0x08250001;
	public static final int MMA_SV_CMD_PIP_SIZE                                  = 0x08260001;
	public static final int MMA_SV_CMD_PIP_PBP_MAIN_SOURCE                       = 0x08270001;
	public static final int MMA_SV_CMD_PIP_PBP_SUB_SOURCE                        = 0x08280001;
	public static final int MMA_SV_CMD_IMAGE_SHIFT_ON_OFF                        = 0x08290001;
	public static final int MMA_SV_CMD_IMAGE_SHIFT_LEFT                          = 0x082A0001;
	public static final int MMA_SV_CMD_IMAGE_SHIFT_RIGHT                         = 0x082B0001;
	public static final int MMA_SV_CMD_IMAGE_SHIFT_UP                            = 0x082C0001;
	public static final int MMA_SV_CMD_IMAGE_SHIFT_DOWN                          = 0x082D0001;
	public static final int MMA_SV_CMD_IMAGE_SHIFT_RESET                         = 0x082E0000;
	public static final int MMA_SV_CMD_DISPLAY_FUNCTION_DISABLED                 = 0x08FF0010;

	// System Image Quality
	public static final int MMA_SIQ_CMD_BRIGHTNESS                               = 0x0C010002;
	public static final int MMA_SIQ_CMD_CONTRAST                                 = 0x0C020002;
	public static final int MMA_SIQ_CMD_SATURATION                               = 0x0C030002;
	public static final int MMA_SIQ_CMD_HUE                                      = 0x0C040002;
	public static final int MMA_SIQ_CMD_COLOR_TEMPERATURE                        = 0x0C050001;
	public static final int MMA_SIQ_CMD_COLOR_TEMP_READ                          = 0x0C060002;
	public static final int MMA_SIQ_CMD_COLOR_TEMP_GREEN                         = 0x0C070002;
	public static final int MMA_SIQ_CMD_COLOR_TEMP_BLUE                          = 0x0C080002;
	public static final int MMA_SIQ_CMD_WALL_COLOR                               = 0x0C090001;
	public static final int MMA_SIQ_CMD_GAMMA                                    = 0x0C0A0002;
	public static final int MMA_SIQ_CMD_COLOR_TEMP_RED_GAIN                      = 0x0C0B0002;
	public static final int MMA_SIQ_CMD_COLOR_TEMP_GREEN_GAIN                    = 0x0C0C0002;
	public static final int MMA_SIQ_CMD_COLOR_TEMP_BLUE_GAIN                     = 0x0C0D0002;
	public static final int MMA_SIQ_CMD_COLOR_TEMP_RED_OFFSET                    = 0x0C0E0002;
	public static final int MMA_SIQ_CMD_COLOR_TEMP_GREEN_OFFSET                  = 0x0C0F0002;
	public static final int MMA_SIQ_CMD_COLOR_TEMP_BLUE_OFFSET                   = 0x0C100002;
	public static final int MMA_SIQ_CMD_SHARPNESS_LEVEL                          = 0x0C110002;
	public static final int MMA_SIQ_CMD_BRILLIANT_COLOR                          = 0x0C120001;
	public static final int MMA_SIQ_CMD_CM_PRIMARY_COLOR                         = 0x0C130001;
	public static final int MMA_SIQ_CMD_CM_HUE                                   = 0x0C140002;
	public static final int MMA_SIQ_CMD_CM_SATURATION                            = 0x0C150002;
	public static final int MMA_SIQ_CMD_CM_GAIN                                  = 0x0C160002;
	public static final int MMA_SIQ_CMD_WHITE_INTENSITY                          = 0x0C170002;
	public static final int MMA_SIQ_CMD_PICTURE_MODE                             = 0x0C180001;
	public static final int MMA_SIQ_CMD_REFERENCE_MODE                           = 0x0C190001;
	public static final int MMA_SIQ_CMD_SAVE_PICTURE_MODE                        = 0x0C1A0001;
	public static final int MMA_SIQ_CMD_WB_R_GAIN                                = 0x0C1B0002;
	public static final int MMA_SIQ_CMD_WB_G_GAIN                                = 0x0C1C0002;
	public static final int MMA_SIQ_CMD_WB_B_GAIN                                = 0x0C1D0002;
	public static final int MMA_SIQ_CMD_PICTURE_FUNCTION_DISABLED                = 0x0CFF0010;

	// Audio
	public static final int MMA_AU_CMD_MUTE                                      = 0x01010001;
	public static final int MMA_AU_CMD_VOLUME                                    = 0x01020002;
	public static final int MMA_AU_CMD_MICROPHONE_VOLUME                         = 0x01030002;
	public static final int MMA_AU_CMD_AUDIO_RING_TONE                           = 0x01040001;
	public static final int MMA_AU_CMD_AUDIO_INPUT_SOURCE                        = 0x01050001;
	public static final int MMA_AU_CMD_TREBLE                                    = 0x01060002;
	public static final int MMA_AU_CMD_BASS                                      = 0x01070002;
	public static final int MMA_AU_CMD_MICROPHONE                                = 0x01080001;
	public static final int MMA_AU_CMD_SPEAKER                                   = 0x01090001;
	public static final int MMA_AU_CMD_OUTPUT_SOURCE                             = 0x010A0001;
	public static final int MMA_AU_CMD_AUDIO_FUNCTION_DISABLED                   = 0x01FF0010;

	// Key
	public static final int MMA_KEY_CMD_KEYCODE                                  = 0x04010004;

	// Lan
	public static final int MMA_LAN_CMD_LAN_DHCP                                 = 0x02010001;
	public static final int MMA_LAN_CMD_LAN_ADDRESS                              = 0x02020010;
	public static final int MMA_LAN_CMD_LAN_MAC_ADDRESS                          = 0x02030006;
	public static final int MMA_LAN_CMD_LAN_STATUS                               = 0x02040001;
	public static final int MMA_LAN_CMD_LAN_SETTING_APPLY                        = 0x02050000;
	public static final int MMA_LAN_CMD_WIFI_ENABLED                             = 0x02060001;
	public static final int MMA_LAN_CMD_WIFI_STA_SSID                            = 0x02070015;
	public static final int MMA_LAN_CMD_WIFI_STA_KEY                             = 0x02080040;
	public static final int MMA_LAN_CMD_WIFI_STA_DHCP                            = 0x02090001;
	public static final int MMA_LAN_CMD_WIFI_STA_ADDRESS                         = 0x020A0010;
	public static final int MMA_LAN_CMD_WIFI_MAC_ADDRESS                         = 0x020B0006;
	public static final int MMA_LAN_CMD_WIFI_STATUS                              = 0x020C0002;
	public static final int MMA_LAN_CMD_WIFI_STA_CONNECTION                      = 0x020D0000;
	public static final int MMA_LAN_CMD_EMERGENCY_DISPLAY                        = 0x020E0001;
	public static final int MMA_LAN_CMD_EMERGENCY_MSG                            = 0x020F0102;
	public static final int MMA_LAN_CMD_TELNET_CMD                               = 0x02110026;
	public static final int MMA_LAN_CMD_LAN_CTRL                                 = 0x02120002;
	public static final int MMA_LAN_CMD_WIFI_STA_SCAN_AP                         = 0x02130000;
	public static final int MMA_LAN_CMD_WIFI_STA_AP_LIST                         = 0x021400C0;
	public static final int MMA_LAN_CMD_CAST_CODE                                = 0x02150002;
	public static final int MMA_LAN_CMD_WIFI_AP_SETTING_APPLY                    = 0x02160000;
	public static final int MMA_LAN_CMD_NETWORK_CUSTOM                           = 0x02170043;
	public static final int MMA_LAN_CMD_LAN_STANDBY                              = 0x02180001;
	public static final int MMA_LAN_CMD_NETWORK_FUNCTION_DISABLED                = 0x02FF0010;

	// System
	public static final int MMA_SYS_CMD_MENU_POSITION                            = 0x06010001;
	public static final int MMA_SYS_CMD_MENU_DISPLAY_TIME                        = 0x06020002;
	public static final int MMA_SYS_CMD_BLANKING_TIMER                           = 0x06030002;
	public static final int MMA_SYS_CMD_SLEEP_TIMER                              = 0x06040002;
	public static final int MMA_SYS_CMD_AUTO_POWER_OFF                           = 0x06050002;
	public static final int MMA_SYS_CMD_DIRECT_POWER_ON                          = 0x06060001;
	public static final int MMA_SYS_CMD_INSTANT_RESTART                          = 0x06070001;
	public static final int MMA_SYS_CMD_HIGH_ALTITUDE_MODE                       = 0x06080001;
	public static final int MMA_SYS_CMD_AUTO_SOURCE_SEARCH                       = 0x06090001;
	public static final int MMA_SYS_CMD_SPLASH_SCREEN                            = 0x060A0001;
	public static final int MMA_SYS_CMD_LAMP_MODE                                = 0x060B0001;
	public static final int MMA_SYS_CMD_LAMP_HOUR                                = 0x060C0002;
	public static final int MMA_SYS_CMD_BAUD_RATE                                = 0x060D0004;
	public static final int MMA_SYS_CMD_PRESENTATION_TIMER_PERIOD                = 0x060E0002;
	public static final int MMA_SYS_CMD_PRESENTATION_TIMER_DISPLAY               = 0x060F0002;
	public static final int MMA_SYS_CMD_PRESENTATION_TIMER_POSITION              = 0x06100001;
	public static final int MMA_SYS_CMD_PRESENTATION_TIMER_COUNTING_DIRECTION    = 0x06110001;
	public static final int MMA_SYS_CMD_PRESENTATION_SOUND_REMINDER              = 0x06120001;
	public static final int MMA_SYS_CMD_PRESENTATION_ON                            = 0x06130001;
	public static final int MMA_SYS_CMD_POWER_ON_LOCK                            = 0x06140001;
	public static final int MMA_SYS_CMD_MONITOR_OUTPUT                           = 0x06150001;
	public static final int MMA_SYS_CMD_SPLASH_SCREEN_ON_LOCK                    = 0x06160001;
	public static final int MMA_SYS_CMD_PANEL_KEY_LOCK                           = 0x06170001;
	public static final int MMA_SYS_CMD_STANDBY_MICROPHONE                       = 0x06180001;
	public static final int MMA_SYS_CMD_REMOTER_RECEIVER                         = 0x06190001;
	public static final int MMA_SYS_CMD_STANDBY_NETWORK                          = 0x061A0001;
	public static final int MMA_SYS_CMD_BACKGROUND_COLOR                         = 0x061B0001;
	public static final int MMA_SYS_CMD_SIGNAL_POWER_ON_VGA                      = 0x061C0001;
	public static final int MMA_SYS_CMD_PROJECTOR_NAME                           = 0x061D0015;
	public static final int MMA_SYS_CMD_MODEL_NAME                               = 0x061E0015;
	public static final int MMA_SYS_CMD_MAKE_NAME                                = 0x061F0015;
	public static final int MMA_SYS_CMD_DEVICE_TYPE                              = 0x06200015;
	public static final int MMA_SYS_CMD_REVISION                                 = 0x06210015;
	public static final int MMA_SYS_CMD_FW_VERSION_OF_PROJECTOR                  = 0x06220015;
	public static final int MMA_SYS_CMD_FW_VERSION_OF_WEB                        = 0x06230015;
	public static final int MMA_SYS_CMD_FW_VERSION_OF_FACTORY                    = 0x06240015;
	public static final int MMA_SYS_CMD_LANGUAGE                                 = 0x06250001;
	public static final int MMA_SYS_CMD_PROJECTOR_ERROR_STATUS                   = 0x06260004;
	public static final int MMA_SYS_CMD_SOURCE_STATUS                            = 0x06270002;
	public static final int MMA_SYS_CMD_POWER_STATUS                             = 0x06280002;
	public static final int MMA_SYS_CMD_RESET_ALL_SETTINGS                       = 0x06290001;
	public static final int MMA_SYS_CMD_INPUT_SOURCE                             = 0x062A0001;
	public static final int MMA_SYS_CMD_CLOSED_CAPTION_ENABLE                    = 0x062B0001;
	public static final int MMA_SYS_CMD_CLOSED_CAPTION_VERSION                   = 0x062C0001;
	public static final int MMA_SYS_CMD_MENU_TYPE                                = 0x062D0001;
	public static final int MMA_SYS_CMD_SIGNAL_POWER_ON_HDMI                     = 0x062E0001;
	public static final int MMA_SYS_CMD_QUICK_COOLING                            = 0x062F0001;
	public static final int MMA_SYS_CMD_AUTO_BLANK                               = 0x06300001;
	public static final int MMA_SYS_CMD_REMINDER_MESSAGE                         = 0x06310001;
	public static final int MMA_SYS_CMD_FIRMWARE_UPGRADE                         = 0x06320002;
	public static final int MMA_SYS_CMD_PROJCTOR_PASSWORD                        = 0x06330007;
	public static final int MMA_SYS_CMD_PHOTO_SETTING                            = 0x06340002;
	public static final int MMA_SYS_CMD_TIMING_INFO                              = 0x0635000A;
	public static final int MMA_SYS_CMD_VGA_INFO                                 = 0x0636000F;
	public static final int MMA_SYS_CMD_FUNCTION_DISPLAY_RANGE                   = 0x0637000A;
	public static final int MMA_SYS_CMD_MHL_CHARGING                             = 0x06380001;
	public static final int MMA_SYS_CMD_NETWORK_STANDBY_TIMER                    = 0x06390002;
	public static final int MMA_SYS_CMD_AM_UART_STATUS                           = 0x063A0001;
	public static final int MMA_SYS_CMD_HDMI_EQUALIZER                           = 0x063B0002;
	public static final int MMA_SYS_CMD_VIDEO_FREEZE                             = 0x063C0001;
	public static final int MMA_SYS_CMD_FW_VERSION_OF_AM                         = 0x063D000A;
	public static final int MMA_SYS_CMD_USB_5V                                   = 0x063E0001;
	public static final int MMA_SYS_CMD_ADC_CALIBRATION                          = 0x063F0000;
	public static final int MMA_SYS_CMD_ADC_GAIN_AND_OFFSET                      = 0x0640000C;
	public static final int MMA_SYS_CMD_USB_PORT_INFO                            = 0x06410002;
	public static final int MMA_SYS_CMD_PROJECTION_ARGUMENT                      = 0x06420008;
	public static final int MMA_SYS_CMD_NATIVE_RESOLUTION                        = 0x06430004;
	public static final int MMA_SYS_CMD_LAMP_REMINDER                            = 0x06440001;
	public static final int MMA_SYS_CMD_DISPLAY_MODE_LOCK                        = 0x06450001;
	public static final int MMA_SYS_CMD_PROJECTOR_ID                             = 0x06460001;
	public static final int MMA_SYS_CMD_SECURITY_TIMER                           = 0x06470002;
	public static final int MMA_SYS_CMD_SWF_STATUS                               = 0x06480010;
	public static final int MMA_SYS_CMD_AV_MUTE                                  = 0x06490001;
	public static final int MMA_SYS_CMD_HDCP_KEY_STATUS                          = 0x064A0001;
	public static final int MMA_SYS_CMD_SYSTEM_CUSTOM                            = 0x064B0030;
	public static final int MMA_SYS_CMD_MCU_POWER_MANAGEMENT                     = 0x064C0001;
	public static final int MMA_SYS_CMD_MCU_BATTERY_MANAGEMENT                   = 0x064D0003;
	public static final int MMA_SYS_CMD_MCU_IO_CONTROL                           = 0x064E0002;
	public static final int MMA_SYS_CMD_MCU_UPDATE_MANAGEMENT                    = 0x064F0002;
	public static final int MMA_SYS_CMD_HEARTBEAT                                = 0x06500000;
	public static final int MMA_SYS_CMD_VGA_OUT                                  = 0x06510001;
	public static final int MMA_SYS_CMD_VGA2                                     = 0x06520001;
	public static final int MMA_SYS_CMD_INTERACTIVE_SETTING                      = 0x06530001;
	public static final int MMA_SYS_CMD_MMA_OSD_SERVICE                      = 0x06540001;
	public static final int MMA_SYS_CMD_SYSTEM_FUNCTION_DISABLED                 = 0x06FF0010;
	public static final int MMA_ACK_CMD                                          = 0x0FFF0000;


	public static final int MMA_IDXKEY_SETUP = 0x0000;
	public static final int MMA_IDXKEY_ENTER = 0x0001;
	public static final int MMA_IDXKEY_UP = 0x0002;
	public static final int MMA_IDXKEY_DOWN = 0x0003;
	public static final int MMA_IDXKEY_LEFT = 0x0004;
	public static final int MMA_IDXKEY_RIGHT = 0x0005;
	public static final int MMA_IDXKEY_BACK = 0x0006;
	public static final int MMA_IDXKEY_AUTO = 0x0007;
	public static final int MMA_IDXKEY_BLANK = 0x0008;
	public static final int MMA_IDXKEY_INPUT = 0x0009;
	public static final int MMA_IDXKEY_POWER = 0x000A;
	public static final int MMA_IDXKEY_FREEZE = 0x000B;
	public static final int MMA_IDXKEY_MIC_VOLUME_UP = 0x000C;
	public static final int MMA_IDXKEY_MIC_VOLUME_DOWN = 0x000D;
	public static final int MMA_IDXKEY_VOL_UP = 0x000E;
	public static final int MMA_IDXKEY_VOL_DOWN = 0x000F;
	public static final int MMA_IDXKEY_DIGITAL_ZOOM_IN = 0x0010;
	public static final int MMA_IDXKEY_DIGITAL_ZOOM_OUT = 0x0011;
	public static final int MMA_IDXKEY_HDMI1 = 0x0012;
	public static final int MMA_IDXKEY_HDMI2 = 0x0013;
	public static final int MMA_IDXKEY_VGA1 = 0x00014;
	public static final int MMA_IDXKEY_VGA2 = 0x0015;
	public static final int MMA_IDXKEY_CVBS = 0x0016;
	public static final int MMA_IDXKEY_CVBS2 = 0x0017;
	public static final int MMA_IDXKEY_RESERVE = 0x0018;
	public static final int MMA_IDXKEY_RESERVE2 = 0x0019;
	public static final int MMA_IDXKEY_MULTI_MEDIA = 0x001A;
	public static final int MMA_IDXKEY_EZCAST = 0x001B;
	public static final int MMA_IDXKEY_MIRACAST = 0x001C;
	public static final int MMA_IDXKEY_EZWIRE = 0x001D;
	public static final int MMA_IDXKEY_USB_CONNECTION = 0x001E;
	public static final int MMA_IDXKEY_PAGE_UP = 0x001F;
	public static final int MMA_IDXKEY_PAGE_DOWN = 0x0020;
	public static final int MMA_IDXKEY_ASPECT = 0x0021;
	public static final int MMA_IDXKEY_MUTE = 0x0022;
	public static final int MMA_IDXKEY_DIGITAL_ZOOM_PANNING_UP = 0x0023;
	public static final int MMA_IDXKEY_DIGITAL_ZOOM_PANNING_DOWN = 0x0024;
	public static final int MMA_IDXKEY_DIGITAL_ZOOM_PANNING_LEFT = 0x0025;
	public static final int MMA_IDXKEY_DIGITAL_ZOOM_PANNING_RIGHT = 0x0026;
	public static final int MMA_IDXKEY_RESYNC = 0x0027;
	public static final int MMA_IDXKEY_TEST_PATTERN = 0x0028;
	public static final int MMA_IDXKEY_CAPTURE = 0x0029;
	public static final int MMA_IDXKEY_PICTURE = 0x002A;
	public static final int MMA_IDXKEY_SMART_ECO = 0x002B;
	public static final int MMA_IDXKEY_TEACHING_TEMPLAT = 0x002C;
	public static final int MMA_IDXKEY_3D_SYNC_INVERT = 0x002D;
	public static final int MMA_IDXKEY_POWER_ON = 0x002E;
	public static final int MMA_IDXKEY_POWER_OFF = 0x002F;
	public static final int MMA_IDXKEY_MENU_HIDE = 0x0030;
	public static final int MMA_IDXKEY_MENU_DISPLAY = 0x0031;
	public static final int MMA_IDXKEY_QUICK_INSTALL = 0x0032;
	public static final int MMA_IDXKEY_SKIP_PREV = 0x0033;
	public static final int MMA_IDXKEY_PLAY_PAUSE = 0x0034;
	public static final int MMA_IDXKEY_SKIP_NEXT = 0x0035;
	public static final int MMA_IDXKEY_REWIND = 0x0036;
	public static final int MMA_IDXKEY_STOP = 0x0037;
	public static final int MMA_IDXKEY_FAST_FORWARD = 0x0038;
	public static final int MMA_IDXKEY_NETWORK_SETTING = 0x0039;
	public static final int MMA_IDXKEY_VK_INC = 0x003A;
	public static final int MMA_IDXKEY_VK_DEC = 0x003B;
	public static final int MMA_IDXKEY_HK_INC = 0x003C;
	public static final int MMA_IDXKEY_HK_DEC = 0x003D;
	public static final int MMA_IDXKEY_KS_RESET = 0x003E;
	public static final int MMA_IDXKEY_DEL = 0x003F;
	public static final int MMA_IDXKEY_MODE = 0x0040;
	public static final int MMA_IDXKEY_TIME = 0x0041;

	public mma_protocol_param_define() {
	}

	//必须提供一个名为CREATOR的static final属性 该属性需要实现android.os.Parcelable.Creator<T>接口
	public static final Creator<mma_protocol_param_define> CREATOR = new Creator<mma_protocol_param_define>() {
		@Override
		public mma_protocol_param_define createFromParcel(Parcel source) {
			return new mma_protocol_param_define(source);
		}

		@Override
		public mma_protocol_param_define[] newArray(int size) {
			return new mma_protocol_param_define[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	private mma_protocol_param_define(Parcel source) {
	}

	//注意写入变量和读取变量的顺序应该一致 不然得不到正确的结果
	@Override
	public void writeToParcel(Parcel dest, int flags) {
	}

	//注意读取变量和写入变量的顺序应该一致 不然得不到正确的结果
	public void readFromParcel(Parcel source) {
	}


	public static int get_command_id(int combine_id) {
		return (combine_id>>16)&0xFFFF;
	}

	public static int get_data_len(int combine_id) {
		return combine_id&0xFFFF;
	}

    public static final int combine_id_read(int combine_id) {
        return (combine_id&0xFFFF0000)|0x80000000;
    }
}
