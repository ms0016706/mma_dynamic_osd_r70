package com.coretronic.mma_dynamicosd_r70.ui;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.coretronic.mma_dynamicosd_r70.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class mini_PickerController extends ViewController {
    private String[] values;
    private int currentIndex;
    private PickerController pickerController;

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.current)
    TextView current;
    @BindView(R.id.left_arrow)
    View leftButton;
    @BindView(R.id.right_arrow)
    View rightButton;

    public mini_PickerController(Context context,
                                 final PickerController pickerController,
                                 final String title, final String[] values, final int initial) {
        super(View.inflate(context, R.layout.mini_picker, null));
        ButterKnife.bind(this, view);

        this.pickerController = pickerController;
        this.values = values;
        this.currentIndex = initial;

        if (title == null) {
            this.title.setVisibility(View.GONE);
        } else {
            this.title.setText(title);
        }

        if (initial == 0)
            leftButton.setVisibility(View.INVISIBLE);
        if (initial == values.length - 1)
            rightButton.setVisibility(View.INVISIBLE);

        this.current.setText(values[initial]);

        view.setNextFocusUpId(view.getId());
    }

    @OnClick(R.id.left_arrow)
    public void onClickLeft() {
        if (currentIndex > 0) {
            currentIndex--;
            current.setText(values[currentIndex]);

            if (currentIndex == 0) {
                leftButton.setVisibility(View.INVISIBLE);
                rightButton.setVisibility(View.VISIBLE);
            } else
                rightButton.setVisibility(View.VISIBLE);

            pickerController.onClickLeft();

            if (!view.hasFocus())
                view.requestFocus();
        }
    }

    @OnClick(R.id.right_arrow)
    public void onClickRight() {
        if (currentIndex < values.length - 1) {
            currentIndex++;
            current.setText(values[currentIndex]);

            if (currentIndex == values.length - 1) {
                leftButton.setVisibility(View.VISIBLE);
                rightButton.setVisibility(View.INVISIBLE);
            } else
                leftButton.setVisibility(View.VISIBLE);

            pickerController.onClickRight();

            if (!view.hasFocus())
                view.requestFocus();
        }
    }
}