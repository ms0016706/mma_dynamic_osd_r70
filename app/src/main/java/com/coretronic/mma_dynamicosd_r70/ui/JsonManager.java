package com.coretronic.mma_dynamicosd_r70.ui;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import android_serialport_api.sample.mma_handshake.mma_protocol_param_define;

public class JsonManager {
    private static JsonManager instance;
    private static final String TAG = JsonManager.class.getSimpleName();
    private JSONArray mOsdTree, mObjectList, mInfo, mChildArray;
    private String mOEM;
    private int mObject_List_Index;

    private JsonManager() {
    }

    private String getAllStorageLocationsList() {
        List<String> mountsList = new ArrayList<>();
        try {
            File mountFile = new File("/proc/mounts");
            if (mountFile.exists()) {
                Scanner scanner = new Scanner(mountFile);
                while (scanner.hasNext()) {
                    String line = scanner.nextLine();

                    if (line.contains("/storage/")) {
                        String[] lineElements = line.split(" ");
                        String location = lineElements[1];
                        if (!location.contains("emulated") && !location.contains("self"))
                            mountsList.add(location);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mountsList.size() > 0)
            return mountsList.get(0);
        else
            return null;
    }

    public static synchronized JsonManager getInstance() {
        if (instance == null) {
            instance = new JsonManager();
        }
        return instance;
    }

    public void parserOsdStruct(Context context) throws Exception {
        JSONObject jsonObject = null;
        String usbpath = getAllStorageLocationsList();
        Log.d(TAG, "usbpath -> " + usbpath);
        String datapath = "/" + usbpath + "/Database/mma_osd_database.json";
        File file = new File(datapath);

        if (file.exists()) {
            if (file != null) {
                FileInputStream stream = new FileInputStream(file);
                String jsonStr;

                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

                jsonStr = Charset.defaultCharset().decode(bb).toString();
                jsonObject = new JSONObject(jsonStr);
            }
        } else {
            InputStream inputStream = context.getAssets().open("mma_osd_database_demo.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            String TreeJson = new String(buffer, "UTF-8");
            jsonObject = new JSONObject(TreeJson);
        }

        mInfo = jsonObject.getJSONArray("Info");
        mOEM = mInfo.getJSONObject(0).getString("OEM");
        Log.d(TAG, "mInfo -> " + mInfo);
        Log.d(TAG, "mOEM -> " + mOEM);

        mOsdTree = jsonObject.getJSONArray("OSDTree");
        mObjectList = jsonObject.getJSONArray("ObjectList");

        Log.d(TAG, "mOsdTree -> " + mOsdTree);
        Log.d(TAG, "mObjectList -> " + mObjectList);
    }

    public void foundChildArray(JSONArray OsdTree, String layer_uid) throws Exception {
        for (int treeIndex = 0; treeIndex < OsdTree.length(); treeIndex++) {
            JSONObject treeObject = OsdTree.getJSONObject(treeIndex);
            Iterator iterator = treeObject.keys();

            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                if (key.equals("SelfId")) {
                    if (treeObject.getString(key).equals(layer_uid)) {
                        if (treeObject.has("ChildId")) {
                            mChildArray = treeObject.getJSONArray("ChildId");
                            break;
                        }
                    }
                } else {
                    foundChildArray(treeObject.getJSONArray(key), layer_uid);
                }
            }
        }
    }

    public JSONObject getAttribute(String uid) throws Exception {
        JSONObject attribute = null;
        for (int object_list_index = 0; object_list_index < mObjectList.length(); object_list_index++) {
            if (uid.equals(mObjectList.getJSONObject(object_list_index).getString("UID"))) {
                attribute = mObjectList.getJSONObject(object_list_index);
                mObject_List_Index = object_list_index;
                break;
            }
        }
        return attribute;
    }

    public int[] getLayerCmd(JSONArray childArray) throws Exception {
        int[] layer_cmd;
        int[] valid_layer_cmd;
        int valid_layer_cmd_index = 0;
        int layer_cmd_send_number = 0;

        layer_cmd = new int[childArray.length()];
        for (int child = 0; child < childArray.length(); child++) {
            String self_id = childArray.getJSONObject(child).getString("SelfId");

            for (int arrayIndex = 0; arrayIndex < mObjectList.length(); arrayIndex++) {
                if (self_id.equals(mObjectList.getJSONObject(arrayIndex).getString("UID"))) {
                    layer_cmd[child] = Integer.parseInt(mObjectList.getJSONObject(arrayIndex).getString("CommandId"), 16);
                    if ((layer_cmd[child] != 0) && (layer_cmd[child] == ((mma_protocol_param_define.MMA_SIQ_CMD_PICTURE_MODE >> 16) & 0xFFFF))) {
                        layer_cmd_send_number++;
                        System.out.printf("======layer_cmd = 0x%x\n", layer_cmd[child]);
                        Log.d(TAG, "arrayIndex -> " + arrayIndex);
                    }
                    break;
                }
            }
        }

        valid_layer_cmd = new int[layer_cmd_send_number];
        for (int i = 0; i < layer_cmd.length; i++) {
            if ((layer_cmd[i] != 0) && (layer_cmd[i] == ((mma_protocol_param_define.MMA_SIQ_CMD_PICTURE_MODE >> 16) & 0xFFFF))) {
                valid_layer_cmd[valid_layer_cmd_index++] = layer_cmd[i];
            }
        }
        return valid_layer_cmd;
    }

    public boolean isRadioMenu(JSONArray childArray) throws Exception {
        String uid, menuType = null;
        uid = childArray.getJSONObject(1).getString("SelfId");

        for (int object_list_index = 0; object_list_index < mObjectList.length(); object_list_index++) {
            if (uid.equals(mObjectList.getJSONObject(object_list_index).getString("UID"))) {
                menuType = mObjectList.getJSONObject(object_list_index).getString("MenuType");
                break;
            }
        }

        if (menuType.equals("Radio"))
            return true;
        else
            return false;
    }

    public JSONArray getChildArray() {
        return mChildArray;
    }

    public int getObjectListIndex() {
        return mObject_List_Index;
    }

    public JSONArray getOsdTree() {
        return mOsdTree;
    }

    public JSONArray getObjectList() {
        return mObjectList;
    }

    public JSONArray getInfo() {
        return mInfo;
    }

    public String getOEM() {
        return mOEM;
    }
}
