package com.coretronic.mma_dynamicosd_r70.ui;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.coretronic.mma_dynamicosd_r70.MainCallback;
import com.coretronic.mma_dynamicosd_r70.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PickerController extends ViewController {
    private String[] values;
    private int currentIndex;
    private MainCallback mCallback;
    private int object_list_index;

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.current)
    TextView current;
    @BindView(R.id.left_arrow)
    View leftButton;
    @BindView(R.id.right_arrow)
    View rightButton;

    public PickerController(Context context, String title, String[] values, int initial, final MainCallback callback, final int index) {
        super(View.inflate(context, R.layout.menu_picker, null));
        ButterKnife.bind(this, view);

        this.mCallback = callback;
        this.values = values;
        this.currentIndex = initial;
        this.object_list_index = index;

        if (title == null)
            this.title.setVisibility(View.GONE);
        else
            this.title.setText(title);

        this.current.setText(values[initial]);

        if (initial == 0)
            leftButton.setVisibility(View.INVISIBLE);
        else if (initial == values.length - 1)
            rightButton.setVisibility(View.INVISIBLE);

        view.setNextFocusUpId(view.getId());

        addOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    mCallback.checkMenuFocus(hasFocus);
            }
        });
    }

    @OnClick(R.id.left_arrow)
    public void onClickLeft() {
        if (currentIndex > 0) {
            currentIndex--;
            current.setText(values[currentIndex]);

            if (currentIndex == 0) {
                leftButton.setVisibility(View.INVISIBLE);
                rightButton.setVisibility(View.VISIBLE);
            } else
                rightButton.setVisibility(View.VISIBLE);

            if (!view.hasFocus())
                view.requestFocus();

            mCallback.item_apply(view.getTag().toString(), object_list_index, currentIndex);
        }
    }

    @OnClick(R.id.right_arrow)
    public void onClickRight() {
        if (currentIndex < values.length - 1) {
            currentIndex++;
            current.setText(values[currentIndex]);

            if (currentIndex == values.length - 1) {
                leftButton.setVisibility(View.VISIBLE);
                rightButton.setVisibility(View.INVISIBLE);
            } else
                leftButton.setVisibility(View.VISIBLE);

            if (!view.hasFocus())
                view.requestFocus();

            mCallback.item_apply(view.getTag().toString(), object_list_index, currentIndex);
        }
    }

    @Override
    public void setEnabled(boolean isEnabled) {
        view.setEnabled(isEnabled);
        view.setFocusable(isEnabled);
        view.setFocusableInTouchMode(isEnabled);
        view.setClickable(isEnabled);

        title.setEnabled(isEnabled);
        current.setEnabled(isEnabled);
    }

    public int getCurrentIndex() {
        return currentIndex;
    }
}
