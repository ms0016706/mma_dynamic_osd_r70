package com.coretronic.mma_dynamicosd_r70.ui;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.coretronic.mma_dynamicosd_r70.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuController extends ViewController {

    @BindView(R.id.function_list)
    ViewGroup itemsWrapper;

    @BindView(R.id.function_content)
    ViewGroup contentWrapper;

    private List<ViewController> items = new ArrayList<>();
    private boolean isInLayer;
    private View last_view;

    public MenuController(Context context, @LayoutRes int layout, boolean inLayer) {
        super(View.inflate(context, layout, null));
        isInLayer = inLayer;
        ButterKnife.bind(this, view);
    }

    public void addItem(ViewController item) {
        last_view = item.getView();
        last_view.setId(View.generateViewId());

        if (items.isEmpty()) {
            if (!isInLayer)
                last_view.setNextFocusUpId(View.NO_ID);
            else
                last_view.setNextFocusUpId(last_view.getId());
        } else {
            final View oldLast = items.get(items.size() - 1).getView();
            oldLast.setNextFocusDownId(last_view.getId());
        }

        items.add(item);
        itemsWrapper.addView(last_view);
    }

    public void addItem(RadioGroup radioGroup) {
        last_view = radioGroup;
        last_view.setId(View.generateViewId());

        itemsWrapper.addView(radioGroup);
    }

    public void last_item_set_next_focus_down_id() {
        if (!items.isEmpty()) {
            last_view.setNextFocusDownId(last_view.getId());
        }
    }
}
