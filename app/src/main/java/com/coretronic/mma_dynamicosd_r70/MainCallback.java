package com.coretronic.mma_dynamicosd_r70;

public interface MainCallback {
    void item_apply(String uid, int object_list_index, int valueIndex);
    void item_apply_seekbar(String uid, int object_list_index, String value);
    void checkMenuFocus(boolean hasFocus);
}
