package com.coretronic.mma_dynamicosd_r70;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.coretronic.mma_dynamicosd_r70.ui.JsonManager;
import com.coretronic.mma_dynamicosd_r70.ui.LensPickerController;
import com.coretronic.mma_dynamicosd_r70.ui.MenuController;
import com.coretronic.mma_dynamicosd_r70.ui.PickerController;
import com.coretronic.mma_dynamicosd_r70.ui.mini_PickerController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import android_serialport_api.sample.ISerialPortService;
import android_serialport_api.sample.IServiceCallbackListener;
import android_serialport_api.sample.mma_handshake.mma_protocol_param_define;

public class MainActivity extends Activity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final boolean LAYOUT_TEST = true;

    private static final int SERVICE_RESPONSE_HANDLE_PICTURE_MODE = 0;
    private static final String ACTION_BIND_SERVICE = "android_serialport_api.sample.SerialPortService";

    private ISerialPortService mIMyService;
    private String remote_callback_cookie;
    private String wait_complete_layout_id;
    private int[] current_layout_cmd_id;

    private boolean osd_menu_change_focus_avoid = false;
    private boolean mIsInLayer;

    private ArrayList<String> mParentList = new ArrayList();
    private JSONArray mOsdTree, mObjectList;
    private LinearLayout mOsd, mOsd_main_menu, mOsd_sub_menu;
    private RelativeLayout mOsd_relative;
    private TextView mOsd_title;
    private String mLastMenuImage;
    private Context mCtx;

    public MainCallback mMainCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!LAYOUT_TEST) {
            remote_callback_cookie = this.getPackageName() + "." + this.getLocalClassName();
            Intent intentService = new Intent(ACTION_BIND_SERVICE);
            intentService.setAction("android_serialport_api.sample.SerialPortService");
            intentService.setPackage("android_serialport_api.sample");
            intentService.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            MainActivity.this.bindService(intentService, mServiceConnection, Context.BIND_AUTO_CREATE);
        }

        mCtx = this;
        setContentView(R.layout.projector_setup);
        mOsd = findViewById(R.id.osd);
        mOsd_main_menu = findViewById(R.id.osd_main_menu);
        mOsd_sub_menu = findViewById(R.id.osd_sub_menu);
        mOsd_title = findViewById(R.id.osd_title);
        mOsd_relative = findViewById(R.id.osd_relative);

        if (LAYOUT_TEST) {
            try {
                JsonManager.getInstance().parserOsdStruct(mCtx);
                mOsdTree = JsonManager.getInstance().getOsdTree();
                mObjectList = JsonManager.getInstance().getObjectList();
                createOsdMenuImage(mOsdTree);

                for (int i = 0; i < mOsd_main_menu.getChildCount(); i++) {
                    final View view = mOsd_main_menu.getChildAt(i);
                    view.setOnFocusChangeListener(menuFocusChangeListener);
                    view.setOnClickListener(menuClickListener);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        mMainCallback = new MainCallback() {
            @Override
            public void item_apply(String uid, int object_list_index, int valueIndex) {
                try {
                    String menuType = mObjectList.getJSONObject(object_list_index).getString("MenuType");
                    int cmd_id = Integer.parseInt(mObjectList.getJSONObject(object_list_index).getString("CommandId"), 16);
                    String value = mObjectList.getJSONObject(object_list_index).getJSONArray("List").getJSONObject(valueIndex).getString("value");

                    Log.d(TAG, "in item_apply ->");
                    Log.d(TAG, "cmd_id: " + cmd_id);
                    Log.d(TAG, "value: " + value);
                    mma_osd_item_apply(cmd_id, menuType, value);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void item_apply_seekbar(String uid, int object_list_index, String value) {
                try {
                    String menuType = mObjectList.getJSONObject(object_list_index).getString("MenuType");
                    int cmd_id = Integer.parseInt(mObjectList.getJSONObject(object_list_index).getString("CommandId"), 16);

                    mma_osd_item_apply(cmd_id, menuType, value);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void checkMenuFocus(boolean hasFocus) {
                Log.d(TAG, "checkMenuFocus -> " + mLastMenuImage);

                for (int i = 0; i < mOsd_main_menu.getChildCount(); i++) {
                    if (mOsd_main_menu.getChildAt(i).getTag().equals(mLastMenuImage)) {
                        if (!mOsd_main_menu.getChildAt(i).isSelected())
                            mOsd_main_menu.getChildAt(i).setSelected(true);
                    }
                }
            }
        };
    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            Toast.makeText(MainActivity.this, "onServiceConnected", Toast.LENGTH_SHORT).show();
            mIMyService = ISerialPortService.Stub.asInterface(service);

            try {
                //注册回调
                mIMyService.mma_register_listener(mListener, remote_callback_cookie);

                JsonManager.getInstance().parserOsdStruct(mCtx);
                mOsdTree = JsonManager.getInstance().getOsdTree();
                mObjectList = JsonManager.getInstance().getObjectList();
                createOsdMenuImage(mOsdTree);

                for (int i = 0; i < mOsd_main_menu.getChildCount(); i++) {
                    final View view = mOsd_main_menu.getChildAt(i);
                    view.setOnFocusChangeListener(menuFocusChangeListener);
                    view.setOnClickListener(menuClickListener);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Toast.makeText(MainActivity.this, "onServiceDisconnected", Toast.LENGTH_SHORT).show();
            mIMyService = null;
        }
    };

    private IServiceCallbackListener.Stub mListener = new IServiceCallbackListener.Stub() {
        @Override
        public void onRespond(byte[] data, int identify_id, byte ack_status) throws RemoteException {
            int ret;
            System.out.printf("IServiceCallbackListener ret = 0x%x\n", identify_id);
            System.out.printf("IServiceCallbackListener 0x7FFF ret = 0x%x\n", identify_id & 0x7FFF);
            System.out.printf("mma_protocol_param_define.MMA_SIQ_CMD_PICTURE_MODE >> 16 ret = 0x%x\n", mma_protocol_param_define.MMA_SIQ_CMD_PICTURE_MODE >> 16);
            System.out.printf("ack_status = 0x%x\n", ack_status);

            // ignore cmd_id rw bit
            switch (identify_id & 0x7FFF) {
                case (mma_protocol_param_define.MMA_SIQ_CMD_PICTURE_MODE >> 16): {
                    if (null != data) {
                        Message msg = uiHandler.obtainMessage();
                        Bundle bundle = new Bundle(1);
                        bundle.putInt("response_case", SERVICE_RESPONSE_HANDLE_PICTURE_MODE);
                        bundle.putByteArray("picture_mode_data", data);
                        msg.setData(bundle);
                        uiHandler.sendMessage(msg);
                    } else
                        Log.d(TAG, "onRespond data is null");
                    break;
                }
            }

            System.out.printf("onRespond identify_id = 0x%x, ack_status = 0x%x \n", identify_id, ack_status);
            if (null != data) {
                Log.d("resp", "receive message from service data length: " + data.length);
                for (int i = 0; i < data.length; i++) {
                    if (i % 8 == 0)
                        System.out.printf("\n");

                    System.out.printf("0x%x, ", data[i]);
                }
                System.out.printf("\n");
            } else {
                Log.d(TAG, "receive message from service data is null ");
            }
        }
    };

    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle b = msg.getData();
            int response_case = b.getInt("response_case", -1);

            switch (response_case) {
                case SERVICE_RESPONSE_HANDLE_PICTURE_MODE: {
                    byte[] data = b.getByteArray("picture_mode_data");

                    System.out.printf("data[0] = %x\n", data[0]);

                    int value = ((data[0] & 0xFF)
                            | ((data[0] & 0xFF) << 8)
                            | ((data[0] & 0xFF) << 16)
                            | ((data[0] & 0xFF) << 24));

                    Log.d(TAG, "value is -> " + value);

                    if (!LAYOUT_TEST)
                        mma_osd_create_sub_menu(String.valueOf(value));
                    break;
                }

                default:
                    break;
            }
        }
    };

    private View.OnFocusChangeListener menuFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            // when create or back sub menu, because remove all view action will make focus change is true,
            // but the behavior is not that we hope, so we need to avoid it.
            if (osd_menu_change_focus_avoid) {
                osd_menu_change_focus_avoid = false;
                return;
            }

            if (hasFocus) {
                wait_complete_layout_id = view.getTag().toString();
                mma_osd_sub_menu_request(view.getTag().toString(), view);
            }
        }
    };

    private View.OnClickListener menuClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            wait_complete_layout_id = view.getTag().toString();
            mma_osd_sub_menu_request(view.getTag().toString(), view);
        }
    };

    private void createOsdMenuImage(JSONArray OsdTree) throws Exception {
        if (!JsonManager.getInstance().getOEM().equals("Optoma")) {
            mOsd.setOrientation(LinearLayout.HORIZONTAL);
            mOsd_main_menu.setOrientation(LinearLayout.VERTICAL);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mOsd_sub_menu.getLayoutParams();
            params.leftMargin = (int) getResources().getDimension(R.dimen.margin_18dp);
            mOsd_sub_menu.setLayoutParams(params);
        }

        for (int treeIndex = 0; treeIndex < mOsdTree.length(); treeIndex++) {
            ImageButton imageButton = new ImageButton(this);
            LinearLayout.LayoutParams imageButton_params = new LinearLayout.LayoutParams(
                    (int) getResources().getDimension(R.dimen.main_menu_image_size),
                    (int) getResources().getDimension(R.dimen.main_menu_image_size));

            if (treeIndex > 0) {
                if (!JsonManager.getInstance().getOEM().equals("Optoma"))
                    imageButton_params.topMargin = (int) getResources().getDimension(R.dimen.margin_18dp);
                else
                    imageButton_params.leftMargin = (int) getResources().getDimension(R.dimen.margin_18dp);
            }

            imageButton.setLayoutParams(imageButton_params);
            imageButton.setBackground(getDrawable(R.drawable.icon_bg));
            imageButton.setClickable(true);
            imageButton.setFocusable(true);
            imageButton.setFocusableInTouchMode(true);

            String uid = OsdTree.getJSONObject(treeIndex).getString("SelfId");
            String imageName = null, title = null;
            for (int i = 0; i < mObjectList.length(); i++) {
                if (mObjectList.getJSONObject(i).getString("UID").equals(uid)) {
                    title = mObjectList.getJSONObject(i).getString("Title");
                    imageName = mObjectList.getJSONObject(i).getJSONArray("List").getJSONObject(0).getString("Image");
                    break;
                }
            }

            imageButton.setTag(uid);
            imageButton.setContentDescription(title);
            imageButton.setImageResource(this.getResources().getIdentifier(imageName,
                    "drawable", this.getPackageName()));

            mOsd_main_menu.addView(imageButton);
        }
    }

    private void mma_osd_sub_menu_request(final String uid, final View view) {
        Log.d(TAG, " in mma_osd_sub_menu_request ->");
        for (int index = 0; index < mObjectList.length(); index++) {
            try {
                if (mObjectList.getJSONObject(index).getString("UID").equals(view.getTag().toString())) {
                    JsonManager.getInstance().foundChildArray(mOsdTree, uid);
                    JSONArray childArray = JsonManager.getInstance().getChildArray();
                    current_layout_cmd_id = JsonManager.getInstance().getLayerCmd(childArray);

                    if (current_layout_cmd_id.length == 0)
                        break;

                    if (!LAYOUT_TEST)
                        mma_osd_information_request(current_layout_cmd_id);
                    else
                        mma_osd_create_sub_menu("0");

                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(mOsd_title.getText());
                    stringBuilder.append(" > ");
                    stringBuilder.append(view.getContentDescription());
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void mma_osd_information_request(int[] current_layout_cmd_id) {
        Log.d(TAG, "in mma_osd_information_request cmd length -> " + current_layout_cmd_id.length);
        byte[] data = new byte[1];
        data[0] = (byte) Integer.parseInt("0", 10);

        for (int i = 0; i < current_layout_cmd_id.length; i++) {
            try {
                mIMyService.mma_send_set_command_aidl(current_layout_cmd_id[i], data.length, data, 1, remote_callback_cookie);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    private void mma_osd_item_apply(final int cmd_id, final String menu_type, final String value) {
        try {
            if (mIMyService != null) {
                byte[] data;

                switch (menu_type) {
                    case "Radio":
                    case "List":
                    case "LensList": {
                        data = new byte[1];
                        data[0] = (byte) Integer.parseInt(value, 10);
                    }
                    break;

                    case "SeekBar": {
                        data = new byte[2];
                        short val = (short) Integer.parseInt(value, 10);

                        data[0] = (byte) (val & 0xff);
                        data[1] = (byte) ((val >> 8) & 0xff);
                    }
                    break;

                    case "Toggle": {
                        data = new byte[1];
                        data[0] = (byte) Integer.parseInt(value, 10);
                    }
                    break;

                    default:
                        return;
                }

                mIMyService.mma_send_set_command_aidl(cmd_id, data.length, data, 0, remote_callback_cookie);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void mma_osd_create_sub_menu(String value) {
        JSONArray childArray;
        View subView;
        ViewGroup parent = findViewById(R.id.function_container);
        String uid = wait_complete_layout_id;

        try {
            mLastMenuImage = uid;
            mIsInLayer = false;
            osd_menu_change_focus_avoid = true;

            mParentList.clear();
            mParentList.add(uid);

            JsonManager.getInstance().foundChildArray(mOsdTree, uid);
            childArray = JsonManager.getInstance().getChildArray();
            subView = createMenuItem(childArray, mIsInLayer, value);
            subView.setTag(uid);

            for (int i = 0; i < mOsd_main_menu.getChildCount(); i++) {
                if (!mOsd_main_menu.getChildAt(i).getTag().toString().equals(mLastMenuImage))
                    if (mOsd_main_menu.getChildAt(i).isSelected())
                        mOsd_main_menu.getChildAt(i).setSelected(false);
            }

            parent.removeAllViews();
            parent.addView(subView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private View createMenuItem(JSONArray childArray, boolean inLayer, String value) throws Exception {
        final MenuController menu = new MenuController(this, R.layout.menu_panel, inLayer);

        for (int childIndex = 0; childIndex < childArray.length(); childIndex++) {

            final String uid = childArray.getJSONObject(childIndex).getString("SelfId");
            final JSONObject attribute = JsonManager.getInstance().getAttribute(uid);
            final String title = attribute.getString("Title");
            final ArrayList<String> List = new ArrayList();
            final int object_list_index = JsonManager.getInstance().getObjectListIndex();
            int current_value = 0;

            switch (attribute.getString("MenuType")) {
                case "List":
                    Log.d(TAG, "value is -> " + value);
                    current_value = Integer.parseInt(value, 10);
                    Log.d(TAG, "current_value -> " + current_value);

                    for (int arrayIndex = 0; arrayIndex < attribute.getJSONArray("List").length(); arrayIndex++) {
                        List.add(attribute.getJSONArray("List").getJSONObject(arrayIndex).getString("text"));
                    }
                    final PickerController pickerController = new PickerController(
                            this, title, List.toArray(new String[0]), current_value, mMainCallback, object_list_index);
                    pickerController.getView().setTag(uid);
                    pickerController.setEnabled(true);

                    pickerController.getView().setOnKeyListener(new View.OnKeyListener() {
                        @Override
                        public boolean onKey(View view, int keyCode, KeyEvent event) {
                            if (event.getAction() != KeyEvent.ACTION_DOWN)
                                return false;

                            if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
                                pickerController.onClickLeft();
                            } else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
                                pickerController.onClickRight();
                            } else
                                return false;
                            return true;
                        }
                    });

                    pickerController.getView().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d(TAG, "List Click ->");
                            final mini_PickerController mini_pickerController = new mini_PickerController(
                                    mCtx, pickerController, title, List.toArray(new String[0]),
                                    pickerController.getCurrentIndex());
                            mini_pickerController.getView().setTag(uid);
                            mini_pickerController.getView().setOnKeyListener(new View.OnKeyListener() {
                                @Override
                                public boolean onKey(View v, int keyCode, KeyEvent event) {
                                    Log.d(TAG, "mini_pickerController onKey -> " + keyCode);
                                    if (event.getAction() != KeyEvent.ACTION_DOWN)
                                        return false;

                                    if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
                                        pickerController.onClickLeft();
                                        mini_pickerController.onClickLeft();
                                    } else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
                                        pickerController.onClickRight();
                                        mini_pickerController.onClickRight();
                                    } else if (keyCode == KeyEvent.KEYCODE_BACK) {
                                        osd_menu_change_focus_avoid = true;
                                        mini_pickerController.getView().setVisibility(View.GONE);
                                        mOsd.setVisibility(View.VISIBLE);
                                        pickerController.getView().requestFocus();
                                    } else
                                        return false;

                                    return true;
                                }
                            });

                            mini_pickerController.getView().findViewById(R.id.title).setOnClickListener(
                                    new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            osd_menu_change_focus_avoid = true;
                                            mini_pickerController.getView().setVisibility(View.GONE);
                                            mOsd.setVisibility(View.VISIBLE);
                                            pickerController.getView().requestFocus();
                                        }
                                    });

                            mOsd_relative.addView(mini_pickerController.getView());
                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                                    (int) getResources().getDimension(R.dimen.mini_control_width),
                                    (int) getResources().getDimension(R.dimen.mini_control_height));
                            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                            mini_pickerController.getView().setLayoutParams(layoutParams);
                            mOsd.setVisibility(View.INVISIBLE);
                            mini_pickerController.getView().requestFocus();
                        }
                    });

                    menu.addItem(pickerController);
                    break;

                case "LensList":
                    for (int arrayIndex = 0; arrayIndex < attribute.getJSONArray("List").length(); arrayIndex++) {
                        List.add(attribute.getJSONArray("List").getJSONObject(arrayIndex).getString("text"));
                    }

                    final LensPickerController lensPickerController = new LensPickerController(
                            this, title, List.toArray(new String[0]), current_value, mMainCallback, object_list_index);
                    lensPickerController.getView().setTag(uid);
                    lensPickerController.setEnabled(true);

                    lensPickerController.getView().setOnKeyListener(new View.OnKeyListener() {
                        @Override
                        public boolean onKey(View view, int keyCode, KeyEvent event) {
                            if (event.getAction() != KeyEvent.ACTION_DOWN)
                                return false;

                            if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT)
                                lensPickerController.onClickLeft();
                            else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT)
                                lensPickerController.onClickRight();
                            else
                                return false;

                            return true;
                        }
                    });

                    lensPickerController.getView().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d(TAG, "List Click ->");
                            lensPickerController.onClick();
                        }
                    });

                    menu.addItem(lensPickerController);
                    break;
            }

            if (childIndex == childArray.length() - 1)
                menu.last_item_set_next_focus_down_id();
        }
        return menu.getView();
    }
}
