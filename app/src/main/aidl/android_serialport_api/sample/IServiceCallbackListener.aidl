// IServiceCallbackListener.aidl
package android_serialport_api.sample;

interface IServiceCallbackListener {
    void onRespond(in byte[] data, int identify_id, byte ack_status);
}