// ISerialPortService.aidl
package android_serialport_api.sample;

import android_serialport_api.sample.IServiceCallbackListener;

interface ISerialPortService {
    int mma_send_set_command_aidl(int cmd_id, int len, in byte[] data, int rw, String cookie);
    void mma_register_listener(IServiceCallbackListener listener, String cookie);
    void mma_unregister_listener(IServiceCallbackListener listener);
}
